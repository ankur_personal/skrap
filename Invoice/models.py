from django.conf import settings
from django.db import models
from django.db.models import Q
from django.utils import timezone
from django.urls import reverse
# from Expense.models import *
# Create your models here.

User = settings.AUTH_USER_MODEL


class Invoice(models.Model):
    # id          = models.IntegerField(primary_key=True)
    expense            = models.IntegerField()
    invoice_date       = models.DateField()
    invoice_period     = models.DateField()
    invoice_cost       = models.FloatField()


    def __str__(self):
        return str(self.expense) + "_" + str(self.id)

    def get_absolute_url(self):
        return reverse("invoice-create", kwargs={"pk": str(self.id)})
