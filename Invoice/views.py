from django.shortcuts import render
from django.shortcuts import render, get_object_or_404
from django.urls import reverse
from django.views.generic import (
    CreateView,
    DetailView,
    ListView,
    UpdateView,
    ListView,
    DeleteView
)
from django.shortcuts import get_list_or_404, get_object_or_404
from django.conf import settings
from django.utils.dateparse import parse_date
import datetime
from .models import Invoice
from .forms import InvoiceCreateForm
# Create your views here.


class InvoiceCreateView(CreateView):
    form_class = InvoiceCreateForm
    template_name = 'Invoice/create_invoice.html'
    queryset = Invoice.objects.all()

    def get_context_data(self, **kwargs):
        expense_id=self.kwargs.get("pk")
        period = self.kwargs.get("pe")
        context = super().get_context_data(**kwargs)
        # context['invoice_jan'] =obj.get_invoice(1)
        context['expense_id'] = expense_id
        context['period'] = period
        return context

    def form_valid(self, form):
        form.instance.expense = self.kwargs.get("pk")
        return super().form_valid(form)

    def get_success_url(self):
        return reverse("expense-detail", kwargs={"pk": str(self.kwargs.get("pk"))})

class InvoiceDeleteView(DeleteView):

    def get_object(self):
        id_ = self.kwargs.get("itd")
        return get_object_or_404(Invoice, id=id_)

    def get_success_url(self):
        return reverse("expense-detail", kwargs={"pk": str(self.kwargs.get("pk"))})
