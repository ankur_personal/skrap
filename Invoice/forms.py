from django import forms

from .models import Invoice

class InvoiceCreateForm(forms.ModelForm):
    class Meta:
        model = Invoice
        fields = [
        'invoice_date',
        'invoice_period',
        'invoice_cost',
        ]

        # help_texts = {
        #     'invoice_date': '(YYYY-MM-DD)',
        # }
