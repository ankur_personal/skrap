Installation process


Windows:

Install pip (If you dont have already) : python get-pip.py
Install Virtualenv (If you dont have already): pip install virtualenv

Create a folder “Skrap”
Open terminal

	cd Skrap
	virtualenv -p python3 . 
	.\Scripts\activate
	pip install django==2.2
	git clone https://gitlab.com/ankur_personal/skrap.git
	
	cd skrap
	python manage.py migrate
	python manage.py createsuperuser
	python manage.py runserver 
	
	
	
Everytime you close the terminal and open again:

    cd Skrap
    .\Scripts\activate
    cd skrap
    python manage.py runserver 




Mac:

Create a folder “Skrap”
Open terminal
	cd Skrap
	virtualenv -p python3 . 
	source bin/activate (.\Scripts\activate)
	pip install django==2.2
	git clone https://gitlab.com/ankur_personal/skrap.git
	
	cd skrap
	python manage.py migrate
	python manage.py createsuperuser
	python manage.py runserver 
	


