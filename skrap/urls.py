"""skrap URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.conf import settings
from django.urls import path, re_path, include
from Company.views import *
from Expense.views import *
from Invoice.views import *
urlpatterns = [
    path('admin/', admin.site.urls),
    path('', SkrapHomeView.as_view(), name='home'),
    path('expense/', ExpenseListView.as_view(), name='expense-list'),
    path('expense/add/', ExpenseCreateView.as_view(), name='expense-create'),
    path('expense/<int:pk>', ExpenseDetailView.as_view(), name='expense-detail'),
    path('expense/<int:pk>/delete/', ExpenseDeleteView.as_view(), name='expense-delete'),
    path('invoice/add/<int:pk>/<str:pe>', InvoiceCreateView.as_view(), name='invoice-create'),
    path('invoice/add/<int:pk>/', InvoiceCreateView.as_view(), name='invoice-create-2'),
    path('invoice/delete/<int:pk>/<int:itd>', InvoiceDeleteView.as_view(), name='invoice-delete')

]
