from django.conf import settings
from django.db import models
from django.db.models import Q
from django.utils import timezone
from django.urls import reverse
from Company.models import Company
from Invoice.models import Invoice
import datetime
# Create your models here.

User = settings.AUTH_USER_MODEL


class Expense(models.Model):
    # id          = models.IntegerField(primary_key=True)
    unit_type = (
    (1, "Day"),
    (2, "Week"),
    (3, "Month"),
    (4, "Year"))
    company            = models.ForeignKey(Company, on_delete=models.CASCADE)
    name               = models.CharField(max_length=120)
    unit_cost          = models.FloatField()
    unit               = models.IntegerField(choices=unit_type, default=3)
    invoice_date       = models.DateField()



    def __str__(self):
        return self.name

    def get_unit_cost(self):
        return "€" + str(self.unit_cost) + " per " + self.unit_type[self.unit-1][1]

    def get_absolute_url(self):
        return reverse("expense-detail", kwargs={"pk": str(self.id)})

    def get_invoice(self, period):
        q = Invoice.objects.filter(expense=self.id)
        mn = q.filter(invoice_period__month = period)
        return mn

    def get_invoice_till_now(self):
        current_month = datetime.date.today().month
        result = []
        i = 1
        while i <= current_month:
            result.append(self.get_invoice(i))
            i += 1
        return result
