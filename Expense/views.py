from django.shortcuts import render, get_object_or_404
from django.urls import reverse
# from django.views.generic.detail import DetailView
# from django.views.generic.list import ListView
# from django.views.generic.edit import CreateView
# from django.views.generic.base import TemplateView
from django.views.generic import (
    CreateView,
    DetailView,
    ListView,
    UpdateView,
    ListView,
    DeleteView
)
import calendar
from django.shortcuts import get_list_or_404, get_object_or_404
from django.conf import settings
from .models import *
from .forms import *
from Company.models import Company
class ExpenseListView(ListView):
    model = Expense
    def get_queryset(self):
        return Expense.objects.filter(company__name = "Catapult")

    # def get_context_data(self, **kwargs):
    #     context = super().get_context_data(**kwargs)
    #     context['company name'] = Article.objects.all()[:5]
    #     return context

class ExpenseDetailView(DetailView):
    model = Expense
    def get_context_data(self, **kwargs):
        id=self.kwargs.get("pk")
        obj = Expense.objects.get(id=id)
        context = super().get_context_data(**kwargs)
        # context['invoice_jan'] =obj.get_invoice(1)
        months = [x for x in calendar.month_name if x]
        invoice_w_months = []
        i = 0
        for invoice in obj.get_invoice_till_now():
            invoice_w_months.append((months[i], invoice))
            i+=1
        context['invoices'] = invoice_w_months
        return context

class ExpenseCreateView(CreateView):
    form_class = ExpenseCreateForm
    template_name = 'Expense/create_expense.html'
    queryset = Expense.objects.all()

    def form_valid(self, form):
        form.instance.company = Company.objects.get(name="Catapult")
        return super().form_valid(form)

    def get_success_url(self):
        return reverse('expense-list')

class ExpenseDeleteView(DeleteView):
    template_name = 'Expense/expense_delete.html'

    def get_object(self):
        id_ = self.kwargs.get("pk")
        return get_object_or_404(Expense, id=id_)

    def get_success_url(self):
        return reverse('expense-list')
