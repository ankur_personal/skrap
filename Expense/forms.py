from django import forms

from .models import Expense

class ExpenseCreateForm(forms.ModelForm):
    class Meta:
        model = Expense
        fields = [
        'name',
        'unit_cost',
        'unit',
        'invoice_date'
        ]

        help_texts = {
            'invoice_date': '(YYYY-MM-DD)',
        }
