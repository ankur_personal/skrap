from django.conf import settings
from django.db import models
from django.db.models import Q
from django.utils import timezone
from django.urls import reverse

# Create your models here.

User = settings.AUTH_USER_MODEL


class Company(models.Model):
    # id          = models.IntegerField(primary_key=True)
    name               = models.CharField(max_length=120)
    num_employees      = models.IntegerField()

    def __str__(self):
        return self.name

    # def get_absolute_url(self):
    #     return reverse("bone-list-child", kwargs={"id": str(self.id)})
