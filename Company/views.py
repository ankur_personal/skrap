from django.shortcuts import render
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView
from django.views.generic.base import TemplateView
from django.shortcuts import get_list_or_404, get_object_or_404
from django.conf import settings


class SkrapHomeView(TemplateView):
    template_name = 'Company/sitehome.html'

    # def get_context_data(self, **kwargs):
    #     context = super().get_context_data(**kwargs)
    #     context['company name'] = Article.objects.all()[:5]
    #     return context
